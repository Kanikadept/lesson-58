import React, {useState} from 'react';
import './App.css';
import Modal from "./UI/Modal/Modal";
import Alert from "./UI/Alert/Alert";

const App = () => {

    const [showState, setShowSate] = useState(false);
    const [alertState, setAlertState] = useState(false);

    const showHandler = () => {
        setShowSate(true);
    }

    const closeHandler = () => {
        setShowSate(false);
    }

    const alertOpenHandler = () => {
        setAlertState(true);
    }

    const alertCloseHandler = () => {
        setAlertState(false);
    }

    return <div className="App">
        <Modal show={showState} close={closeHandler}>
            <h3>A Title</h3>
            <p>Some content</p>
        </Modal>
        <Alert open={alertCloseHandler} showAlert={alertState} dismiss={alertCloseHandler}/>
        <button onClick={showHandler}>Pop up</button>
        <button onClick={alertOpenHandler}>Alert</button>
    </div>
};

export default App;
