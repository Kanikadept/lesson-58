import React from 'react';
import './Modal.css';
import BackDrop from "../BackDrop/BackDrop";

const Modal = props => {
    return (
       <>
           <BackDrop show={props.show} onClick={props.close}/>
           <div className="Modal"
                style={{
                    transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                    opacity: props.show ? '1' : '0'
                }}
           >
               {props.children}
               <button className="close-modal" onClick={props.close}>Close</button>
           </div>
       </>
    );
};

export default Modal;