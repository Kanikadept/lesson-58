import React from 'react';
import './Alert.css';

const Alert = ({type, dismiss, showAlert}) => {
    return (
        <div className="alert" style={{
            transform: showAlert ? 'translateY(0)' : 'translateY(-100vh)',
            opacity: showAlert ? '1' : '0'
        }}>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores, voluptas?</p>
            <button onClick={dismiss}>X</button>
        </div>
    );
};

export default Alert;